require('dotenv').config();

const express = require('express');
const bodyparser = require('body-parser');

const { v4: uuidv4 } = require('uuid');
const Client = require('ftp');
const fs = require('fs');

const app = express();

app.use(bodyparser.json({ limit:'1000mb'}));

app.post('/upload_image', (req, res) => {
    if(req.query.token == process.env.ADMINPASS) {
        if(req.body.image) {
            let data = req.body.image;
            data = data.replace(/^data:image\/\w+;base64,/, '');
            const u = uuidv4();
            const buffer = new Buffer(data,'base64');
            const c = new Client();
            c.on('ready', () => {
                c.put(buffer, '/www/kamiya-' + u + '.png', (err) => {
                    c.end();
                    if (err) {
                        console.log(err);
                        res.send({
                            uuid: u,
                            url: 'Failed to upload this content,contact the admin.'
                        });
                        return;
                    };
                    res.send({
                        uuid: u,
                        url: process.env.PUBLIC_URL + '/kamiya-' + u + '.png'
                    });
                    console.log('File uploaded successfully ' + process.env.PUBLIC_URL + '/kamiya-' + u + '.png');
                });
            });
            c.connect({
                host: process.env.FTPSERVER,
                port: process.env.FTPPORT,
                user: process.env.FTPUSER,
                password: process.env.FTPPASS
            })
        }
        else res.send({success: false});
    }
    else res.send({success: false});
});

app.listen(process.env.PORT);